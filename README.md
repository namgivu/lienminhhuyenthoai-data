We collect all visible data on the internet for leagueoflegend champion/item/rune/etc. 

We then cook the data so that to extract the best setting/playplan for one selected champion - user of our webapp will have to select their favourite champion, and one only.

Mongo database with python stack ref. https://gitlab.com/namgivu/pymongo-start

The built data will be used for the api at https://gitlab.com/namgivu/lienminhhuyenthoai-api which then used in the webapp at https://gitlab.com/namgivu/lienminhhuyenthoai

We move our development works to this repo https://gitlab.com/namgivu/lol - no commits for this repo for now
